{ haskellPackages ? (import <nixpkgs> {}).haskellPackages }:
let
  inherit (haskellPackages) cabal cabalInstall_1_18_0_3
aeson async base64Bytestring aes irc lens
 MissingH network pbkdf random regexPosix yaml
    ;
in 

cabal.mkDerivation (self: {
  pname = "irk";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  buildDepends = [
    aeson async base64Bytestring aes irc lens MissingH network
    pbkdf random regexPosix yaml
  ];
  buildTools = [ cabalInstall_1_18_0_3 ];
  meta = {
    homepage = "http://bitbucket.org/jhinkle/irk";
    description = "An IRC encryption (AES) proxy";
    license = self.stdenv.lib.licenses.bsd3;
    platforms = self.ghc.meta.platforms;
  };
})
