{-# LANGUAGE OverloadedStrings,FlexibleContexts #-}

module Main where

import Irk.Config
import Irk.ConfigFile
import Irk.Crypto

import Control.Monad
-- concurrency stuff
import qualified Control.Concurrent.Async as A
import Control.Concurrent
import Control.Exception.Base

import           Codec.Crypto.AES (Direction(..))

import qualified Data.ByteString.Char8 as BS

import System.Environment (getArgs)

import System.IO
import Text.Printf

import Network

import Control.Lens

-- | Take two handles and map an IO function over lines
hMapLinesIO :: Handle -> Handle -> (BS.ByteString -> IO [BS.ByteString]) -> IO ()
hMapLinesIO handleIn handleOut f = do
    hSetNewlineMode handleOut universalNewlineMode
    hSetBuffering handleOut LineBuffering
    filtMessageLoop
  where
    filtMessageLoop = do
        m <- BS.hGetLine handleIn
        fms <- f m
        -- output each message in the result
        forM fms $ BS.hPutStrLn handleOut
        filtMessageLoop

main :: IO ()
main = withSocketsDo $ do
    -- read config file
    args <- getArgs
    cf <- loadConfig args 

    chanFiltKeys <- forM (cf^.channels) $ \cc -> do
        printf "Generating key from passphrase for filter: %s ..." (cc^.filt)
        hFlush stdout
        let k = genKey $! cc^.crypto
        putStrLn $ seq k "DONE" -- forces computation here
        return (BS.pack (cc^.filt), k)

    sock <- listenOn $ PortNumber $ fromIntegral (cf^.network.local.port)
    printf "Listening on port %d\n" (cf^.network.local.port)
    forever $ do
        (handleLocal, hostLocal, portLocal) <- accept sock
        printf "Accepted connection from %s:%s\n" hostLocal (show portLocal)

        -- now connect to remote host
        handleRemote <- connectTo (cf^.network.remote.host) $ PortNumber $ fromIntegral (cf^.network.remote.port)

        -- finally ensures handle is closed no matter what happens in talk
        forkIO $ void $ A.concurrently
            (hMapLinesIO handleRemote handleLocal
                (mapCryptIRC chanFiltKeys Decrypt))
            (hMapLinesIO handleLocal handleRemote
                (mapCryptIRC chanFiltKeys Encrypt))
                    `finally` hClose handleRemote

