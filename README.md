# irk (formerly hsIRCenc)

IRC encryption proxy based on
[cloaknet](http://www.s1m0n3.org/cloaknet/index.html)

# Requirements

- `pipes-network`
- `pipes-concurrency`
- `async`
- `AES`
- `base64-bytestring`
- `irc`

To install all these in one go just run

```
cabal install --only-dependencies
```

# Installation

To install, you can just run `cabal install`.  If run as a non-root user, this
will install to `~/.cabal/bin`.

# Usage

Right now there's no configuration mechanism, so you'll have to change the
crypto settings in the source code directly and rebuild.

Then just invoke the proxy by running `irk`.  Note this will reside in
`~/.cabal/bin` if you installed using `cabal install`, or if you only run
`cabal build` it will be in `dist/build` under the source directory.

Once the proxy is running it will tell you which port it's listening on.  Then
it's ready for you to connect using your IRC client.  To do this just point your
client to the server at `localhost:9997` (or whatever is specified as the
incoming port).

Once connected, your IRC connection will behave like normal, meaning you can
`/join` new channels as normal.  However, when you send a message, it will be
encrypted and appear as gibberish to other users.  Also, messages you receive
from others will include a warning `!!!PLAINTEXT!!!` indicating that plaintext
messages are coming across the wire in a form that is easily monitored.

Only other users running irk with the same encryption parameters will be
able to communicate normally.  In order to send plaintext, so that others not
running the encryption proxy are able to read your messages, prepend your
message with an underscore (for example `_this is a plaintext message`).  The
underscore will be removed before transmission

# Similar Projects

This project was inspired by [cloaknet](http://www.s1m0n3.org/cloaknet/index.html).
