{-# LANGUAGE OverloadedStrings #-}

module Irk.Crypto  where

import Irk.Config

import Control.Lens

import Control.Exception
import Control.Monad

import Data.String (fromString)
import Data.String.Utils (rstrip)
import Data.List  -- for String processing

import Data.Maybe

import Text.Regex.Posix
import Text.Regex.Posix.ByteString
import System.Path.WildMatch (wildToRegex)

import qualified Network.IRC.Base as IB
import qualified Network.IRC.Parser as IP (decode)

import           Codec.Crypto.AES

import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Base64   as BB

import System.Random (randomIO)

import Crypto.PBKDF.ByteString (sha256PBKDF2)

-- | Space-padding to multiple of 16 length
pad16 :: ByteString -> ByteString
pad16 s | BS.length s `mod` 16 == 0 = s
        | otherwise = pad16 $ s `BS.snoc` ' '

data AESError = InvalidIV | InvalidDataLength deriving Eq
instance Show AESError where
    show InvalidIV = "AES error: invalid IV length"
    show InvalidDataLength = "AES error: invalid data length"

type Key = ByteString
type IV = ByteString

-- | crypt throws on misformatted input. This is a safe version
cryptSafe :: Mode -> Key -> IV -> Direction -> ByteString -> Either AESError ByteString
cryptSafe m k v@(vb) d s
    | BS.length vb /= 16 = Left InvalidIV
    | BS.length s `mod` 16 /= 0 = Left InvalidDataLength
    | otherwise = Right $ crypt' m k v d s

-- | Generate a pseudorandom block to use as an IV
randomBlock :: IO ByteString
randomBlock = fmap BS.pack $ replicateM 16 randomIO

-- | Determine max message length, given headers in message
maxPlainMsgLength :: IB.Message -> Int
maxPlainMsgLength m = 494 - nullLen
  where
    nullMsg = setMsgText m ""
    nullLen = BS.length $ IB.encode nullMsg

{- | Determine max message length before encryption.
This length is determined using the fact that 16 bytes are added to the message
for the IV, before encryption.  After that, the message is base64 encoded and
the "++ " prefix is added.

The encryption step pads up to a multiple of 16 bytes, then preserves that
length

The base64 encoding step transforms length n to length 4*ceil(n/3).
-}
maxCryptMsgLength :: IB.Message -> Int
maxCryptMsgLength m = 16 * ( (floor $ (fromIntegral l - 3) * 0.75) `div` 16 - 2)
  where
    l = maxPlainMsgLength m - 3


{- | Encrypt a message, including ++ and the IV as first block
-}
encryptText :: Key -> ByteString -> ByteString -> Either AESError ByteString
encryptText k v s
    | "_" `BS.isPrefixOf` s = Right $ BS.drop 1 s -- underscore bypasses encryption
    | otherwise = do
        cipher <- cryptSafe CBC k v Encrypt $ pad16 s
        return $ "++ " `BS.append` (BB.encode $ v `BS.append` cipher)

-- | Handle our protocol, retrieve IV and decrypt
decryptText :: Key -> ByteString -> Either AESError ByteString
decryptText k s
    | "++ " `BS.isPrefixOf` s = let
        b = BB.decodeLenient $ BS.drop 3 s
        (v, cipher) = BS.splitAt 16 b in
            cryptSafe CBC k v Decrypt cipher
    | otherwise = return $ "(plain) " `BS.append` s -- unencrypted text sent over the wire

-- | Look for a time-stamp like the one we get from ZNC when playing back a buffer
decryptTextWithTimestamp :: Key -> ByteString -> Either AESError ByteString
decryptTextWithTimestamp k s = case mm of
    Just (_,ts,msg) -> fmap (BS.append ts) $ decryptText k msg
    Nothing -> decryptText k s
  where
    mm :: Maybe (ByteString, ByteString, ByteString)
    mm = s =~~ BS.pack "^\\[[0-9][0-9]:[0-9][0-9]:[0-9][0-9]\\] "

-- | Simply replace the text in a message with |text|
setMsgText :: IB.Message -> ByteString -> IB.Message
setMsgText (IB.Message pref "PRIVMSG" (target:_:xs)) text =
    IB.Message pref "PRIVMSG" (target:text:xs)
setMsgText m _ = m

-- | Handle errors and map onto a message
runErrorMsg :: (Show a) => IB.Message -> Either a ByteString -> IB.Message
runErrorMsg m e = setMsgText m $ either (BS.pack . show) id e 

{- | This performs encryption of a string, splitting if necessary to stay within
IRC message length limits.
-}
encryptSplit :: IB.Message -> Key -> ByteString -> IO [IB.Message]
encryptSplit m k s
    | BS.length s > ml = let (s1, s2) = BS.splitAt ml s in do
        c1 <- encryptSplit m k s1
        c2 <- encryptSplit m k s2
        return $ c1 ++ c2
    | otherwise = do
        rb <- randomBlock
        let c = encryptText k rb s
        return [ runErrorMsg m c ]
  where
    ml = maxCryptMsgLength m

-- | Take a parsed IRC message, a direction, and a ChannelConfig and
-- encrypt/decrypt the message ONLY if the target matches the filter
cryptChanMsg :: IB.Message -> Direction -> (ByteString, Key) -> IO (Maybe [IB.Message])
cryptChanMsg msg@(IB.Message pref "PRIVMSG" (target:text:xs)) dir (f, key)
    | target =~ wildToRegex (BS.unpack f) = case dir of
        Encrypt -> fmap Just $ encryptSplit msg key text
        Decrypt -> return $ Just [ runErrorMsg msg $ decryptTextWithTimestamp key text ]
    | otherwise = return Nothing
cryptChanMsg _ _ _ = return Nothing

-- | Try matching against a list of channels, return original string or
-- crypted string for matching channel
mapCryptIRC :: [(ByteString, Key)] -> Direction -> ByteString -> IO [ByteString]
mapCryptIRC chans dir req = case IP.decode (putNewline req) of
    Nothing -> return [req]
    Just m -> do
        -- try to crypt for each channel config, take first that works
        maybeCrypts <- mapM (cryptChanMsg m dir) chans
        -- take the first Just, if there is one
        let fm = listToMaybe $ catMaybes maybeCrypts
        -- now just encode the thing and send it back
        return $ map (putNewline . IB.encode) $ fromMaybe [m] fm

-- | Tack on a newline if one isn't already present
putNewline :: ByteString -> ByteString
putNewline s = stripNewlines s `BS.append` "\n"

-- | Remove newlines in string
stripNewlines :: ByteString -> ByteString
stripNewlines = (BS.filter . flip BS.notElem) "\r\n"

-- | Generate a key from a passphrase+salt, using 5000 iterations of sha256
-- PBKDF2 to produce a 16 byte key
genKey :: CryptoConfig -> Key
genKey cc = sha256PBKDF2 (BS.pack (cc^.passphrase)) (BS.pack (cc^.salt)) (cc^.numiters) 16
