{-# LANGUAGE OverloadedStrings,TemplateHaskell #-}

module Irk.ConfigFile (
    loadConfig,
    loadYaml
) where

import Irk.Config

import qualified Data.Yaml as Y
import Data.Maybe (listToMaybe)
import Data.Aeson.TH

import Data.ByteString
import qualified Data.ByteString.Char8    as BS

-- use more template haskell to set up JSON instances
$(deriveJSON defaultOptions{fieldLabelModifier=Prelude.tail} ''Cxn)
$(deriveJSON defaultOptions{fieldLabelModifier=Prelude.tail} ''CxnPair)
$(deriveJSON defaultOptions{fieldLabelModifier=Prelude.tail} ''CryptoConfig)
$(deriveJSON defaultOptions{fieldLabelModifier=Prelude.tail} ''ChannelConfig)
$(deriveJSON defaultOptions{fieldLabelModifier=Prelude.tail} ''FullConfig)

-- load a configuration, maybe the default one, given command line args
-- TODO: Use the configfile package instead of yaml
loadConfig :: [String] -> IO FullConfig
loadConfig args = do
    -- get the filename of a config file, if given
    let cffile = args `maybeIndex` 0
    -- if not, we just wnat to use defaultConfig
    case cffile of
         Nothing -> do 
            Prelude.putStr $ "# The following is a sample config:\n" ++ BS.unpack (Y.encode sampleConfig)
            error "Please provide a config file (you can pipe this into a .yml file as a template)"
         Just cf -> loadYaml cf

-- helper function to load up a configuration from a string
loadYaml :: String -> IO FullConfig
loadYaml fp = do
    mval <- Y.decodeFile fp
    case mval of
        Nothing -> error $ "Couldn't load config " ++ show fp
        Just c -> do
            Prelude.putStrLn $ "Loaded " ++ fp
            return c

-- index a list, if that index exists
maybeIndex :: [a] -> Int -> Maybe a
maybeIndex l i = listToMaybe $ Prelude.drop i l

-- Sample
sampleCxnPair :: CxnPair
sampleCxnPair = CxnPair { _local  = Cxn { _host = "127.0.0.1"
                                        , _port = 9997 }
                        , _remote = Cxn { _host = "irc.freenode.net"
                                        , _port = 6667 } }

-- | The passphrase is to be kept private, shared by trusted channel members
-- only (change from these defaults also).  The salt and number of
-- iterations could be public, but it doesn't hurt to keep them secret as
-- well.  Salt should be a pseudorandom string
sampleCrypto :: CryptoConfig
sampleCrypto = CryptoConfig { _passphrase = "super_secret_passphrase"
                             , _salt  = "3u76@B24eFg5c1D9"
                             , _numiters = 5000 }

sampleChannel :: ChannelConfig
sampleChannel = ChannelConfig { _filt="#irk-test", _crypto=sampleCrypto}

sampleConfig :: FullConfig
sampleConfig = FullConfig { _network=sampleCxnPair
                          , _channels=[sampleChannel]}
