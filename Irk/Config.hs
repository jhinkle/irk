{-# LANGUAGE TemplateHaskell #-}

module Irk.Config  where

import Data.ByteString

import Control.Lens

-- Configuration via YAML
    
-- crypto config
-- we're gonna be lame here and just use Strings to represent these, since
-- Crypto.Cipher doesn't export the full interface to Key and IV
-- Instead, we'll need to handle errors and stuff after these are loaded
data CryptoConfig = CryptoConfig {
    _passphrase::String,
    _salt::String,
    _numiters::Int } deriving Show

data ChannelConfig = ChannelConfig {
    _filt::String,
    _crypto::CryptoConfig } deriving Show

-- Need some types for configuration
data Cxn = Cxn {
    _host::String,
    _port::Integer } deriving Show
data CxnPair = CxnPair {
    _local::Cxn,
    _remote::Cxn } deriving Show

data FullConfig = FullConfig {
    _network::CxnPair,
    _channels::[ChannelConfig] } deriving Show

-- Now set up some lenses to access shit in all this
$(makeLenses ''CryptoConfig)
$(makeLenses ''ChannelConfig)
$(makeLenses ''Cxn)
$(makeLenses ''CxnPair)
$(makeLenses ''FullConfig)
